package com.example.practice04;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity2 extends AppCompatActivity {
    GridView gv_img;
    TextView tv_score;
    int score = 0;
    AdapterCustom adapterCustom;
    ArrayList<HinhAnh> arrayList;
    ArrayList<ObjectCeck> arrayCheck;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        tv_score = findViewById(R.id.tv_score);
        tv_score.setText("Score: " + String.valueOf(score));
        gv_img = findViewById(R.id.gv_img);

        arrayList = new ArrayList<>();
        arrayList.add(new HinhAnh(R.drawable.ic_bellsprout, "1"));
        arrayList.add(new HinhAnh(R.drawable.ic_caterpie, "2"));
        arrayList.add(new HinhAnh(R.drawable.ic_container, "10"));
        arrayList.add(new HinhAnh(R.drawable.ic_fight_go, "3"));
        arrayList.add(new HinhAnh(R.drawable.ic_hat_photo_icon, "4"));
        arrayList.add(new HinhAnh(R.drawable.ic_pokemon_icon, "5"));
        arrayList.add(new HinhAnh(R.drawable.ic_pokemon_venonat_icon, "5"));
        arrayList.add(new HinhAnh(R.drawable.ic_zubat_icon, "7"));
        arrayList.add(new HinhAnh(R.drawable.ic_snorlax_icon, "9"));
        arrayList.add(new HinhAnh(R.drawable.ic_squirtle_icon, "9"));
        arrayList.add(new HinhAnh(R.drawable.ic_bellsprout, "1"));
        arrayList.add(new HinhAnh(R.drawable.ic_caterpie, "2"));
        arrayList.add(new HinhAnh(R.drawable.ic_container, "10"));
        arrayList.add(new HinhAnh(R.drawable.ic_fight_go, "3"));
        arrayList.add(new HinhAnh(R.drawable.ic_hat_photo_icon, "4"));
        arrayList.add(new HinhAnh(R.drawable.ic_pokemon_icon, "5"));
        arrayList.add(new HinhAnh(R.drawable.ic_pokemon_venonat_icon, "5"));
        arrayList.add(new HinhAnh(R.drawable.ic_zubat_icon, "7"));
        arrayList.add(new HinhAnh(R.drawable.ic_snorlax_icon, "9"));
        arrayList.add(new HinhAnh(R.drawable.ic_squirtle_icon, "9"));

        arrayCheck = new ArrayList<>();
        Collections.shuffle(arrayList);
        adapterCustom = new AdapterCustom(MainActivity2.this, R.layout.col_items, arrayList);
        adapterCustom.setiOnclick(new AdapterCustom.IOnClick() {
            @Override
            public void clickItemGrid(HinhAnh data, int position) {
                //Toast.makeText(MainActivity2.this, data.getName(), Toast.LENGTH_SHORT).show();
                arrayCheck.add(new ObjectCeck(data.getSrc(), data.getName(), position));
                if (arrayCheck.size() % 2 == 0){
                    if(arrayCheck.get(arrayCheck.size()-2).getName().equals(arrayCheck.get(arrayCheck.size()-1).getName())){
                        //Toast.makeText(MainActivity2.this, "Dung roi", Toast.LENGTH_SHORT).show();
                        score += 10;
                        tv_score.setText("Score: " + String.valueOf(score));
                        new Handler(getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                int indexUpdate = getIndex(arrayCheck.get(arrayCheck.size() -1).name, arrayList);
                                arrayList.get(indexUpdate).setSrc(R.drawable.ic_white);
                                int indexUpdatePre = getIndex(arrayCheck.get(arrayCheck.size() -2).name, arrayList);
                                arrayList.get(indexUpdatePre).setSrc(R.drawable.ic_white);
                                adapterCustom.notifyDataSetChanged();
                            }
                        }, 500);

                        //arrayCheck.get(0).set
                    }else {
                        //Toast.makeText(MainActivity2.this, "Sai roi", Toast.LENGTH_SHORT).show();
                        score -= 1;
                        tv_score.setText("Score: " + String.valueOf(score));
                        new Handler(getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                int indexUpdate = getIndex(arrayCheck.get(arrayCheck.size() -1).name, arrayList);
                                arrayList.get(indexUpdate).setSrc(R.drawable.ic_question_help_icon);
                                int indexUpdatePre = getIndex(arrayCheck.get(arrayCheck.size() -2).name, arrayList);
                                arrayList.get(indexUpdatePre).setSrc(R.drawable.ic_question_help_icon);
                                adapterCustom.notifyDataSetChanged();
                            }
                        }, 500);
                    }
                }
            }
        });
        gv_img.setAdapter(adapterCustom);
    }

    private int getIndex(String name, List<HinhAnh> arrayList){
        for (HinhAnh img : arrayList){
            if (img.getName().equals(name)){
                return arrayList.indexOf(img);
            }
        }
        return -1;
    }
}