package com.example.practice04;

public class ObjectCeck {
    int src;
    String name;
    int pos;


    public int getSrc() {
        return src;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public void setSrc(int src) {
        this.src = src;
    }

    public ObjectCeck(int src, String name, int pos) {
        this.src = src;
        this.name = name;
        this.pos = pos;
    }

    public void setName(String name) {
        this.name = name;
    }



    public String getName() {
        return name;
    }



}
