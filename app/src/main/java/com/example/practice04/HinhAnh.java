package com.example.practice04;

public class HinhAnh {
    private int src;
    private String name;

    public void setSrc(int src) {
        this.src = src;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSrc() {
        return src;
    }

    public String getName() {
        return name;
    }

    public HinhAnh(int src, String name) {
        this.src = src;
        this.name = name;
    }
}
