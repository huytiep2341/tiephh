package com.example.practice04;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AdapterCustom extends BaseAdapter {
    private Context context;
    private int layout;
    ArrayList<ObjectCeck> arrayCheck;
    private List<HinhAnh> hinhAnhList;
    public interface IOnClick{
        void clickItemGrid(HinhAnh data, int position);
    }
    public IOnClick iOnClick;
    public void setiOnclick(IOnClick iOnClick){
        this.iOnClick = iOnClick;
    }

    @Override
    public int getCount() {
        return hinhAnhList.size();
    }

    public AdapterCustom(Context context, int layout, List<HinhAnh> hinhAnhList) {
        this.context = context;
        this.layout = layout;
        this.hinhAnhList = hinhAnhList;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View View, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View = inflater.inflate(layout, null);

        ImageView iv_items = View.findViewById(R.id.iv_item);

        HinhAnh img = hinhAnhList.get(i);
        iv_items.setImageResource(R.drawable.ic_question_help_icon);
        arrayCheck = new ArrayList<>();
        iv_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                iv_items.setImageResource(img.getSrc());
                if(iOnClick != null){
                    iOnClick.clickItemGrid(img , i);
                }
//                arrayCheck.add(new ObjectCeck(img.getSrc(), img.getName(), i));
//                if (arrayCheck.size() % 2 == 0){
//                    if(arrayCheck.get(arrayCheck.size()-2).getName().equals(arrayCheck.get(arrayCheck.size()-1).getName())){
//                        Toast.makeText(context, "Dung roi", Toast.LENGTH_SHORT).show();
//                        hinhAnhList.get(arrayCheck.size()-1).setSrc(R.drawable.ic_white);
//                        hinhAnhList.get(arrayCheck.size()-2).setSrc(R.drawable.ic_white);
//
//                        //arrayCheck.get(0).set
//                    }else {
//                        Toast.makeText(context, "Sai roi", Toast.LENGTH_SHORT).show();
//                        hinhAnhList.get(arrayCheck.size()-1).setSrc(R.drawable.ic_question_help_icon);
//                        hinhAnhList.get(arrayCheck.size()-2).setSrc(R.drawable.ic_question_help_icon);
//
//                    }
//                }
            }
        });


        return View;
    }
}
